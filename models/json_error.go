package models

type JsonError struct {
	Error   int    `json:"error"`
	Message string `json:"message"`
}
